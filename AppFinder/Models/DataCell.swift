//
//  DataCell.swift
//  SearchApp
//
//  Created by Kasey Schlaudt on 6/14/17.
//  Copyright © 2017 Kasey Schlaudt. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class DataCell: UITableViewCell {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var sellerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   func congigureCell(_ appData: AppData) {
        Alamofire.request(appData.logoUrl ).responseImage { response in
            
            if let image = response.result.value {
                
                self.logo.image = image.af_imageAspectScaled(toFit: CGSize(width:self.logo.bounds.width,height:self.logo.bounds.height))
               
            }
    }
        self.label.text = appData.name
        self.sellerName.text = "\(appData.seller)(\(appData.type))"
}
}
