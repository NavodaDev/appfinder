//
//  AppInfoModel.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/4/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import Foundation

struct AppData {
    let name:String
   let  logoUrl:String
    let imageUrl:String
    let seller:String
    let type:String
    let formattedPrice:String
    let userRating:Double
    let primaryGenreName:String
    init(name:String, logoUrl:String, imageUrl:String, seller:String, type:String,formattedPrice:String,userRating:Double,primaryGenreName:String) {

        self.imageUrl = imageUrl
        self.name = name
        self.logoUrl = logoUrl
        self.seller = seller
        self.type = type
        self.formattedPrice = formattedPrice
        self.userRating = userRating
        self.primaryGenreName = primaryGenreName
    }
}
