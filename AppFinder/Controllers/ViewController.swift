//
//  ViewController.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/3/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import UIKit
import  AlamofireImage
import Alamofire


import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!

    let appService = AppService()
    let AppSegueIdentifier = "ShowAppSegue"
   
    
    var filteredData = [AppData]()
    
    var inSearchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
        tableView.delegate = self
        
        tableView.dataSource = self
        
        searchBar.delegate = self
        navigationController?.navigationBar.isHidden = true
        searchBar.returnKeyType = UIReturnKeyType.done
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 83
        
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if inSearchMode {
            
            return filteredData.count
        }
        else {return 0}
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DataCell", for: indexPath) as? DataCell {
            
           
            
            if inSearchMode {
                let appData = filteredData[indexPath.row]
              cell.congigureCell(appData)
            }
            
            return cell
            
        } else {
            
            return UITableViewCell()
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(filteredData.count != 0){
            tableView.deselectRow(at: indexPath, animated: true)
            
            let row = indexPath.row
            print(filteredData[row])
        }
    }
 
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            
            inSearchMode = false
            
            view.endEditing(true)
            
            tableView.reloadData()
            
        } else {
            
            inSearchMode = true
            if((searchBar.text?.count)! >= 4){
               
                getAppData(term: searchBar.text ?? "")
                
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == AppSegueIdentifier,
            let destination = segue.destination as? modalViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            let selectedApp = filteredData[index]
            destination.genrename = selectedApp.primaryGenreName
             destination.typeValue = selectedApp.type
            destination.rating = selectedApp.userRating
             destination.sellerName = selectedApp.seller
             destination.Appname = selectedApp.name
            destination.currencyFormatValue = selectedApp.formattedPrice
             destination.image = selectedApp.imageUrl
        }
    }
    func getAppData(term:String){
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        appService.GetApps(term: term) { (apps, err) in
            self.filteredData = apps
            self.view.endEditing(true)
            
            self.tableView.reloadData()
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
        }
        
    }
 
}



