//
//  modalViewController.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/4/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class modalViewController: UIViewController {

    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var ratings: UILabel!
    @IBOutlet weak var cuurencyBtn: UIButton!
    @IBOutlet weak var seller: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var currencyFormat: UIButton!
    @IBOutlet weak var imageUrl: UIImageView!
    
    var genrename:String = ""
    var typeValue:String = ""
    var rating:Double = 0.0
    var sellerName:String = ""
    var Appname:String = ""
    var currencyFormatValue:String = ""
    var image = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        configerAppData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    @IBAction func dissmiss(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    
    }
    func configerAppData()  {
       print(currencyFormatValue)
        currencyFormat.setTitle(currencyFormatValue, for: [])
        genre.text = genrename
        type.text = typeValue
        seller.text = sellerName
        name.text = Appname
        ratings.text  = "\(rating)"
        Alamofire.request(image ).responseImage { response in
            
            if let image = response.result.value {
                
                self.imageUrl.image = image.af_imageAspectScaled(toFit: CGSize(width:self.imageUrl.bounds.width,height:self.imageUrl.bounds.height))
                
            }
        }
    }


}
@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
}
}
