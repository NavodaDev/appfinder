//
//  AppService.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/4/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import SwiftyJSON

open class AppService {
   
    fileprivate let _webApiHelper = WebApiHelper()
  
    func GetApps(term:String,_ completion: @escaping (_ appData: [AppData], _ error_type:String)->()) {
     
        let headers = [
         
            "Accept": "application/json"
        ]
        
        self._webApiHelper.SendHttpRequest(params: [:] as AnyObject, header_obj: headers, url: "search?term=\(term)e&limit=200&entity=software", http_method: .get) { (response_value, error) in
            print(response_value)
            var apps:[AppData] = []
            
            if(error != "NO_INT"){
              
                var  Name=""
                var  LogoUrl=""
              
                for i in 0..<response_value.count  {
                    
                  
                    if let value = response_value[i]["LogoUrl"].string {
                        LogoUrl = value
                    }
                    if let value = response_value[i]["Name"].string {
                        Name = value
                        
                        
                    }
                  
                    apps.append(AppData(imageUrl: LogoUrl))
                    
                    
                }//for
                completion(apps ,"")
                
                
            }else{
                completion(apps ,"NO_INT")
            }
        }//SendHttpRequest
        
    }}
