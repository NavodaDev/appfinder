//
//  WebApiHelper.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/4/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

open class WebApiHelper
{
    
    //Live URL'S
    var BASE_URL : String = "https://itunes.apple.com/"
    
    
    
    func SendHttpRequest(params: AnyObject, header_obj: [String: String], url:String, http_method:HTTPMethod, completion: @escaping (_ response_value: JSON, _ error_type: String)->()) {
        
        Alamofire.request(BASE_URL + url, method: http_method, parameters: (params as! [String : Any]) , encoding: URLEncoding.default, headers: header_obj).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success( _ ):
                
                if let jsonValue = response.result.value {
                    let json = JSON(jsonValue)
                    completion(json, "")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion(JSON(response.result.value ?? "error"), "NO_INT")
                //"The Internet connection appears to be offline."
                break
                
            }
        }//Alamofire
        
    }//SendHttpRequest
    
  
    
}
