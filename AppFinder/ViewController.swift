//
//  ViewController.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/3/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import UIKit



import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!

    let appService = AppService()
    let AppSegueIdentifier = "ShowAppSegue"
   
    
    var filteredData = [String]()
    
    var inSearchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        tableView.dataSource = self
        
        searchBar.delegate = self
        navigationController?.navigationBar.isHidden = true
        searchBar.returnKeyType = UIReturnKeyType.done
        getAppData()
        
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if inSearchMode {
            
            return filteredData.count
        }
        else {return 0}
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? DataCell {
            
            let text: String!
            
            if inSearchMode {
                
                text = filteredData[indexPath.row]
                cell.congigureCell(text: text)
            }
            
            return cell
            
        } else {
            
            return UITableViewCell()
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(filteredData.count != 0){
            tableView.deselectRow(at: indexPath, animated: true)
            
            let row = indexPath.row
            print(filteredData[row])
        }
    }
 
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            
            inSearchMode = false
            
            view.endEditing(true)
            
            tableView.reloadData()
            
        } else {
            
            inSearchMode = true
        
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == AppSegueIdentifier,
            let destination = segue.destination as? modalViewController,
            let blogIndex = tableView.indexPathForSelectedRow?.row
        {
            // destination.blogName = filteredData[blogIndex]
        }
    }
    func getAppData(){
        appService.GetApps(term: "Games") { (apps, err) in
            
        }
        
    }
 
}



