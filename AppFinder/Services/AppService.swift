//
//  AppService.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/4/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import SwiftyJSON

open class AppService {
    
    fileprivate let _webApiHelper = WebApiHelper()
    
    func GetApps(term:String,_ completion: @escaping (_ appData: [AppData], _ error_type:String)->()) {
        
        let headers = [
            
            "Accept": "application/json"
        ]
        
        self._webApiHelper.SendHttpRequest(params: [:] as AnyObject, header_obj: headers, url: "search?term=\(term)e&limit=200&entity=software", http_method: .get) { (response_value, error) in
            print(response_value)
            var apps:[AppData] = []
            
            if(error != "NO_INT"){
                for i in 0..<response_value["results"].count  {
                    var  name=""
                    var logoUrl=""
                    var imageUrl=""
                    var seller=""
                    let type="App"
                    var formattedPrice=""
                    var userRating = 0.0
                    var primaryGenreName = ""
                    if let value = response_value["results"][i]["artworkUrl100"].string {
                        logoUrl = value
                    }
                    if let value = response_value["results"][i]["trackName"].string {
                        name = value
                        
                    }
                    if let value = response_value["results"][i]["formattedPrice"].string {
                        formattedPrice = value
                    }
                    if let value = response_value["results"][i]["primaryGenreName"].string {
                        primaryGenreName = value
                        
                    }
                    if let value = response_value["results"][i]["averageUserRating"].double {
                        userRating = value
                    }
                    if let value = response_value["results"][i]["sellerName"].string {
                        seller = value
                        
                    }
                    if let value = response_value["results"][i]["artworkUrl512"].string {
                        imageUrl = value
                        
                    }
                    apps.append(AppData(name: name, logoUrl: logoUrl, imageUrl: imageUrl, seller: seller, type: type, formattedPrice: formattedPrice, userRating: userRating, primaryGenreName: primaryGenreName))
                    
                }//for
                completion(apps ,"")
                
            }else{
                completion(apps ,"NO_INT")
            }
        }//SendHttpRequest
        
    }}
