//
//  AppInfoModel.swift
//  AppFinder
//
//  Created by Gayesha Suraweera on 10/4/18.
//  Copyright © 2018 GNSolutions. All rights reserved.
//

import Foundation

struct AppData {
    
   let ImageUrl:String
    init(imageUrl:String) {
        self.ImageUrl = imageUrl
    }
}
